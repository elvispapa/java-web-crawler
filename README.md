# About the Project

A Java application which counts top javascript libraries used in web pages found on Google.


## How to run the application

Just run the WebCrawlerDemo class and search for a term.


## Authors

* **Elvis Papa** (https://www.elvispapa.com)


