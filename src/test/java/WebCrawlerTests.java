import com.GoogleWebCrawler;
import com.URLConnector;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class WebCrawlerTests {

	@Test
	public void extractJavascriptLibraryNamesFromPage() {
		// given
		GoogleWebCrawler crawler = new GoogleWebCrawler(URLConnector.getInstance());
		// and
		String htmlPage = "<html>\n<script src='https://www.domain.com/jquery.js'/>\n" +
				"<script src='https://www.domain.com/bootstrap.js'/>\n" +
				"<script src='https://www.domain.com/vue.js'></script>" +
				"<script> some JS code here..</script>" +
				"</html>";
		List<String> expectedLibraryNames = new ArrayList<>();
		expectedLibraryNames.add("jquery");
		expectedLibraryNames.add("bootstrap");
		expectedLibraryNames.add("vue");

		// then
		assertEquals(expectedLibraryNames, crawler.extractJavascriptLibraryNamesFromPage(htmlPage));
	}

	@Test
	public void extractLibraryNameFromPath() {
		// given
		GoogleWebCrawler crawler = new GoogleWebCrawler(URLConnector.getInstance());
		// and
		String libraryPath = "www.googletagservices.com/tag/scipts/jquery.js";

		// then
		assertEquals("jquery", crawler.extractLibraryNameFromPath(libraryPath));
	}

	@Test
	public void extractLinksFromPage() {
		// given
		GoogleWebCrawler crawler = new GoogleWebCrawler(URLConnector.getInstance());
		// and
		String htmlPage = "<html>" +
				"<div><h3 class=\"r\"><a href=\"/url?q=https://www.domain1.com/&amp;\"><b>Java</b></a></h3></div>" +
				"<div><h3 class=\"r\"><a href=\"/url?q=https://www.domain2.com/&amp;\"><b>Java</b></a></h3></div>" +
				"</html>";
		// and
		List<String> expectedLinks = new ArrayList<>();
		expectedLinks.add("https://www.domain1.com/");
		expectedLinks.add("https://www.domain2.com/");

		// then
		assertEquals(expectedLinks, crawler.extractLinksFromPage(htmlPage));


	}
}
