package com;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BaseWebCrawler {

	public List<String> extractJavascriptLibraryNamesFromPage(final String htmlPage) {
		List<String> libraryNames = new ArrayList<String>();

		final String regex = "<script(?:[^>]*src=['\"]([^'\"]*)['\"][^>]*>|[^>]*>([^<]*)</script>)";
		final Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

		Matcher matcher = pattern.matcher(htmlPage);
		while (matcher.find()) {
			String libraryPath = matcher.group(1);
			if (libraryPath != null) {
				String libraryName = extractLibraryNameFromPath(libraryPath);
				if (libraryName != null) {
					libraryNames.add(libraryName);
				}
			}
		}
		return libraryNames;
	}

	public String extractLibraryNameFromPath(String path) {
		String name = null;
		if (path.contains(".js")) {
			String[] pathParts = path.split("\\.js");
			if (pathParts[0] != null && pathParts[0].contains("/")) {
				String[] mainParts = pathParts[0].split("/");
				name = mainParts[mainParts.length - 1];
			}
		}
		return name;
	}
}
