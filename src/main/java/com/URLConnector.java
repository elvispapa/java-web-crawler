package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class URLConnector {

	private static final String AGENT = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2";

	private static URLConnector instance = new URLConnector();

	private URLConnector() {
	}

	public static URLConnector getInstance() {
		return instance;
	}

	public String buildStringFromInputStream(InputStream inputStream) {
		StringBuilder stringBuilder = new StringBuilder();

		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
		String line;
		try {
			while ((line = bufferedReader.readLine()) != null) {
				stringBuilder.append(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return stringBuilder.toString();
	}

	public String connect(String url) throws IOException {
		final URLConnection connection = new URL(url).openConnection();
		connection.setRequestProperty("User-Agent", AGENT);
		final InputStream stream = connection.getInputStream();

		return buildStringFromInputStream(stream);
	}
}
