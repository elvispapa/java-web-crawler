package com;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GoogleWebCrawler extends BaseWebCrawler implements WebCrawler {

	private final String BASE_SEARCH_URL = "https://www.google.com/search";

	public URLConnector urlConnector;

	public GoogleWebCrawler(URLConnector urlConnector) {
		this.urlConnector = urlConnector;
	}

	@Override
	public String doSearchRequest(String searchTerm, int max) {
		String htmlPage = null;
		try {
			String searchUrl = buildSearchRequestUrl(searchTerm, max);
			htmlPage = urlConnector.connect(searchUrl);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return htmlPage;
	}

	@Override
	public Map<String, String> downloadPagesFromLinks(List<String> links) {
		Map<String, String> pages = new ConcurrentHashMap<>();

		links.parallelStream().forEach((link) -> {
			try {
				String page = urlConnector.connect(link);
				pages.put(link, page);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return pages;
	}

	@Override
	public List<String> extractLinksFromPage(final String htmlPage) {
		List<String> links = new ArrayList<String>();

		String regex1 = "<h3 class=\"r\"><a href=\"/url?q=";
		String regex2 = "\">";
		final Pattern pattern = Pattern.compile(Pattern.quote(regex1) + "(.*?)" + Pattern.quote(regex2));
		final Matcher matcher = pattern.matcher(htmlPage);

		while (matcher.find()) {
			String linkUrl = matcher.group(0).trim();
			// Remove some unnecessary part from the url
			if (linkUrl.contains("/url?q=")) {
				linkUrl = linkUrl.substring(linkUrl.indexOf("/url?q=") + 7);
			}
			if (linkUrl.contains("&amp;")) {
				linkUrl = linkUrl.substring(0, linkUrl.indexOf("&amp;"));
			}
			links.add(linkUrl);
		}
		return links;
	}

	@Override
	public Map<String, List<String>> extractJavascriptLibraryNamesUsedInLinks(List<String> links) {
		Map<String, List<String>> libraryNames = new ConcurrentHashMap<>();
		Map<String, String> linkToPagesMap = downloadPagesFromLinks(links);

		links.parallelStream().forEach((link) -> {
			try {
				String page = linkToPagesMap.get(link);
				if (page != null) {
					List<String> libraryNamesFound = extractJavascriptLibraryNamesFromPage(page);
					libraryNames.put(link, libraryNamesFound);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		return libraryNames;
	}

	@Override
	public Map<String, Integer> getTopJavascriptLibraryNamesUsedInLinks(List<String> links, int max) {
		Map<String, Integer> allLibraryNames = new HashMap<>();
		try {
			Map<String, List<String>> linkToLibraryNamesMap = extractJavascriptLibraryNamesUsedInLinks(links);

			List<String> libraryNames = new ArrayList<>();
			linkToLibraryNamesMap.forEach((k, v) -> libraryNames.addAll(v));

			Set<String> libraries = new HashSet<>(libraryNames);
			for (String library : libraries) {
				int frequency = Collections.frequency(libraryNames, library);
				allLibraryNames.put(library, frequency);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// TODO: sort the map 'allLibraryNames' and return the first 'max' of elements
		return allLibraryNames;
	}

	private String buildSearchRequestUrl(String searchTerm, int max) {
		searchTerm = searchTerm.replaceAll("\\s", "+");
		return BASE_SEARCH_URL + "?q=" + searchTerm + "&num=" + max;
	}
}
