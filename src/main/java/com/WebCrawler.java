package com;

import java.util.List;
import java.util.Map;

public interface WebCrawler {

	String doSearchRequest(String searchTerm, int max);

	List<String> extractLinksFromPage(final String htmlPage);

	Map<String, String> downloadPagesFromLinks(List<String> links);

	Map<String, List<String>> extractJavascriptLibraryNamesUsedInLinks(List<String> links);

	Map<String, Integer> getTopJavascriptLibraryNamesUsedInLinks(List<String> links, int max);
}
