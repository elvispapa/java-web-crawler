package com;

public class CrawlerFactory {

	public WebCrawler getCrawler(String name) {
		if (name == "GOOGLE") {
			return new GoogleWebCrawler(URLConnector.getInstance());
		}
		return null;
	}
}
