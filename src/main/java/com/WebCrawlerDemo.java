package com;

import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class WebCrawlerDemo {

	public static void main(String[] args) {
		try {
			CrawlerFactory crawlerFactory = new CrawlerFactory();
			WebCrawler crawler = crawlerFactory.getCrawler("GOOGLE");

			if (crawler != null) {
				System.out.print("---- Please enter a search term: ");
				Scanner scanner = new Scanner(System.in);
				String searchTerm = scanner.nextLine();

				String page = crawler.doSearchRequest(searchTerm, 10);
				List<String> links = crawler.extractLinksFromPage(page);

				System.out.println("---- The following links were found:");
				for (String link : links) {
					System.out.println(link);
				}

				System.out.println("Searching for javaScript library used in the above pages...please wait...");

				Map<String, Integer> javascriptLibraryNames = crawler.getTopJavascriptLibraryNamesUsedInLinks(links, 5);
				System.out.println("---- The following JavaScript library names were found:");

				for (Map.Entry<String, Integer> library : javascriptLibraryNames.entrySet()) {
					System.out.println("- " + library.getKey() + ": " + library.getValue() + " times");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
